Créer un projet python nom ornitho_test:

Créer un virtualenv

Utiliser Falcon et Gunicorn pour créer:
- Un point d'API GET /status qui retourne un status 200
- Un point d'API POST /{doc_id}/save qui prend un json en entrée et le save sur l'ordinateur dans doc-id.json


Faire tourner le code dans un Docker

Ajouter une DB Mongo dans le handler /status qui fait un count sur n'importe quelle collection avant de retourner un status 200.

Bonus: Utiliser docker-compose pour lancer la DB
